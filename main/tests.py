from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import home, data

class Story8(TestCase):

    def test_story_url_exist(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)

    def test_story_url_dontexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'main/home.html')
    
    def test_header(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Search Book",html_response)
